package com.example.die_dreisten_drei.an2ro.backend_gedoens.EventBus;

/* Created by Robert Neumann on 24.06.2018.
 * Matrikel-Nr.: 15589
 * ToDo:
 */

/**
 * Holds all the local event types that are fired from the {@link com.example.die_dreisten_drei.an2ro.PlayerActivity} to the
 * {@link com.example.die_dreisten_drei.an2ro.backend_gedoens.PlayerService} via the EventBus.
 */

public class SeekbarEvent {
    public static class SeekTo {

        public final int position;

        public SeekTo(int position) {
            this.position = position;
        }
    }
}
