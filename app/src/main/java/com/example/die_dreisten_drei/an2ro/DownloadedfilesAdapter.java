package com.example.die_dreisten_drei.an2ro;
/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.die_dreisten_drei.an2ro.backend_gedoens.Podcast;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.PodcastItem;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

public class DownloadedfilesAdapter extends BaseAdapter implements View.OnClickListener {
    private Activity activity;
    private ArrayList<PodcastItem> data;
    private static LayoutInflater inflater = null;
    private Resources res;
    private PodcastItem podcastItem = null;
    private int i = 0;

    public DownloadedfilesAdapter(Activity _activity, ArrayList<Podcast> _arraylist, Resources _resLocal) {
        this.activity = _activity;
        data = new ArrayList();
        for (Podcast x : _arraylist) {
            if (x.hasDownloads()) {
                for (PodcastItem y : x.getStreams()) {
                    if (y.getOld_url() != null) {
                        if (y.getTitle().contains(Downloadedfiles.searchString)) {
                            if (new File(y.getPath()).exists())
                                data.add(y);
                        }
                    }
                }
            }
        }

        res = _resLocal;
        inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        public TextView dlTitle;
        public TextView dlSize;
        public ImageView podcastImage;
        public ImageButton btnDelete;
        public ImageButton btnPlay;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {
            vi = inflater.inflate(R.layout.df_listlayout, null);

            holder = new ViewHolder();
            holder.dlTitle = (TextView) vi.findViewById(R.id.dlTitle);
            holder.dlSize = (TextView) vi.findViewById(R.id.dlSize);
            holder.podcastImage = (ImageView) vi.findViewById(R.id.podcastImage);
            holder.btnDelete = vi.findViewById(R.id.btnDelete);
//            holder.btnPlay = vi.findViewById(R.id.btnPlay);

            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (data.size() <= 0) {
            holder.dlTitle.setText("Kein Titel");

        } else {
            podcastItem = null;
            podcastItem = data.get(position);

            File audioFile = new File(podcastItem.getPath());


            Picasso.get()
                    .load("file://" + podcastItem.getPodcast_image())
                    .placeholder(R.drawable.placeholder_image)
                    .fit()
                    .into(holder.podcastImage);

            holder.dlSize.setText(Long.toString((audioFile.length() / 1048576)) + " MB");
            holder.dlTitle.setText(podcastItem.getTitle());
            holder.btnDelete.setImageResource(R.drawable.baseline_delete_white_24dp);
//            holder.btnPlay.setImageResource(R.drawable.baseline_arrow_forward_black_24dp);


            holder.btnDelete.setOnClickListener(new OnDeleteButtonClickListener(position));
//            holder.btnPlay.setOnClickListener(new OnPlayButtonClickListener(position));
        }
        return vi;
    }

    private class OnDeleteButtonClickListener implements View.OnClickListener {
        private int pos;

        OnDeleteButtonClickListener(int _position) {
            pos = _position;
        }

        @Override
        public void onClick(View _arg0) {
            PodcastItem tempCastItem = data.get(pos);
            File markedForDeath = new File(tempCastItem.getPath());
            if (markedForDeath.exists())
                markedForDeath.delete();
            tempCastItem.setPath(tempCastItem.getOld_url());
            tempCastItem.setOld_url("");
            ((Downloadedfiles) activity).refreshView();
        }
    }


//    private class OnPlayButtonClickListener implements View.OnClickListener {
//        private int pos;
//
//        OnPlayButtonClickListener(int _position) {
//            pos = _position;
//        }
//
//        @Override
//        public void onClick(View _arg0) {
//            PodcastItem tempItem = data.get(pos);
//            int podcastPosition=0;
//            int itemPosition=0;
//            for (int i =0; i< MainActivity.aboList.size(); i++){
//                if(MainActivity.aboList.get(i).getImage().equals(tempItem.getPodcast_image())){
//                    podcastPosition=i;
//                    for (int j =0; j<MainActivity.aboList.get(i).getStreams().size();j++){
//                        if(MainActivity.aboList.get(i).getStreams().get(j).equals(tempItem)){
//                            itemPosition=j;
//                        }
//                        break;
//                    }
//
//                }
//                break;
//            }
//            ((Downloadedfiles)activity).playItem(podcastPosition, itemPosition);
//        }
//    }

    @Override
    public void onClick(View v) {
    }
}
