package com.example.die_dreisten_drei.an2ro.backend_gedoens;

import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;

import com.example.die_dreisten_drei.an2ro.MainActivity;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

public class RSSRequest {
    public RSSRequest(String _url, Podcast... _updateCast) throws IOException, SAXException {

        //todo auslagern
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);


        URL url = new URL(_url);
        URLConnection connection = url.openConnection();
        connection.setConnectTimeout(0);
        connection.setRequestProperty("UserAgent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.1.4322)");
        connection.setDoInput(true);
        InputStream inStream = connection.getInputStream();
        BufferedReader input = new BufferedReader(new InputStreamReader(inStream));

        String line = "";
        StringBuilder sb = new StringBuilder();
        while ((line = input.readLine()) != null) {
            sb.append(line.replaceAll("&#034;", "\"").replaceAll("&#039;", "\'") + System.lineSeparator());
            if (line.contains("<channel>"))
                sb.append("<an2ropodcasturl>" + _url + "</an2ropodcasturl>" + System.lineSeparator());
        }
        System.setProperty("org.xml.sax.driver", "org.xmlpull.v1.sax2.Driver");
        XMLReader xmlReader = XMLReaderFactory.createXMLReader();
        InputSource inputSource = new InputSource(new StringReader(sb.toString()));
        if (_updateCast.length > 0)
            xmlReader.setContentHandler(new RSSUpdateContentHandler(_updateCast[0]));
        else {
            boolean exists = false;
            for (Object x : MainActivity.aboList) {
                if (((Podcast) x).getUrl().equals(_url))
                    exists = true;
            }
            if (!exists)
                xmlReader.setContentHandler(new RSSContentHandler());
        }
        xmlReader.parse(inputSource);
        connection.getInputStream().close();
        inStream.close();
    }
}


