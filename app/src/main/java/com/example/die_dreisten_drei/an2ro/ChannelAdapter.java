package com.example.die_dreisten_drei.an2ro;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.constraint.Constraints;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.die_dreisten_drei.an2ro.backend_gedoens.Podcast;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.PodcastItem;

import java.util.ArrayList;

public class ChannelAdapter extends BaseAdapter implements View.OnClickListener {

    private Activity activity;
    private ArrayList data;
    private static LayoutInflater inflater = null;
    private Resources res;
    private PodcastItem tempCast = null;
    private int i = 0;
    private int descriptionIndex = -1;

    public ChannelAdapter(Activity _activity, ArrayList<PodcastItem> _arraylist, Resources _resLocal) {
        this.activity = _activity;
        data = new ArrayList();
        for (PodcastItem x : _arraylist)
            if (x.getTitle().toLowerCase().contains(ChannelActivity.searchString.toLowerCase()))
                data.add(x);
        res = _resLocal;
        inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        public TextView tfTitle;
        public TextView tfDuration;
        public ImageView imgDuration;
        public ImageButton btnPlayInLinst;
        public TextView textViewDescription;
        public ImageView imageViewdivider;

    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {
            vi = inflater.inflate(R.layout.podcastitemlayout, null);

            holder = new ViewHolder();
            holder.tfTitle = (TextView) vi.findViewById(R.id.textViewtitle);
            holder.tfDuration = (TextView) vi.findViewById(R.id.textViewDuration);
            holder.imgDuration = (ImageView) vi.findViewById(R.id.imageView);
            holder.btnPlayInLinst = vi.findViewById(R.id.btnPlayInList);
            holder.textViewDescription = vi.findViewById(R.id.textViewDescription);
            //holder.imageViewdivider = vi.findViewById(R.id.imageViewdivider);

            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (data.size() <= 0) {
            holder.tfTitle.setText("Kein Titel");

        } else {
            tempCast = null;
            tempCast = (PodcastItem) data.get(position);

            holder.tfTitle.setText(tempCast.getTitle());
            holder.tfDuration.setText(tempCast.getDuration());
            holder.imgDuration.setImageResource(R.drawable.ic_visibilityinlist);
            holder.btnPlayInLinst.setImageResource(R.drawable.baseline_arrow_forward_black_24dp);
            //holder.imageViewdivider.setBackgroundColor(vi.getResources().getColor(R.color.an2roLight));
            if(descriptionIndex!=position){
                holder.textViewDescription.setText("");
            }else{
                holder.textViewDescription.setPaddingRelative(0,0,0,20);
                holder.textViewDescription.setText(MainActivity.aboList.get(ChannelActivity.Podcastid).getStreams().get(position).getDescription());
                holder.textViewDescription.setTextSize(13);
            }

            holder.btnPlayInLinst.setOnClickListener(new OnPlayButtonClickListener(position));
            vi.setOnClickListener(new OnItemClickListener(position));
        }
        return vi;
    }

    @Override
    public void onClick(View v) {
    }

    private class OnItemClickListener implements View.OnClickListener {
        private int pos;

        OnItemClickListener(int _position) {
            pos = _position;
        }

        @Override
        public void onClick(View _arg0) {
            TextView ItemDesc = _arg0.findViewById(R.id.textViewDescription);
            if (ItemDesc.getText().toString().isEmpty()) {
                ItemDesc.setText(MainActivity.aboList.get(ChannelActivity.Podcastid).getStreams().get(pos).getDescription());
                ItemDesc.setTextSize(13);
                ItemDesc.setPadding(0,0,0,20);
                descriptionIndex=pos;
            } else {
                ItemDesc.setText("");
                ItemDesc.setTextSize(1);
                ItemDesc.setPadding(0,0,0,0);
                descriptionIndex=-1;
            }
        }
    }

    private class OnPlayButtonClickListener implements View.OnClickListener {
        private int pos;

        OnPlayButtonClickListener(int _position) {
            pos = _position;
        }

        @Override
        public void onClick(View _arg0) {
            ChannelActivity sct = (ChannelActivity) activity;
            sct.onItemClick(pos);
        }
    }
}
