package com.example.die_dreisten_drei.an2ro.backend_gedoens;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */
/*
 * https://blog.samuelattard.com/basic-bitmap-caching-in-android/
 * https://stackoverflow.com/questions/12850143/android-basics-running-code-in-the-ui-thread
 * ToDo:
 */
class DownloadImageTask extends AsyncTask<String, String, String> {


    String returnPath = "";
    /**
     * Before starting background thread
     * */

       @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("downloadimg", "onPreExe");
    }

    @Override
    protected String doInBackground(String... _imageURL) {
        int count;

        try {
            String root = Environment.getExternalStorageDirectory().toString();
            URL url = new URL(_imageURL[0]);
            Uri uri = Uri.parse(_imageURL[0]);

            //Datei-Endung holen
            String FileName = uri.getLastPathSegment();
            String FileExt = getMimeType(_imageURL[0]);

            String extension = FileExt.substring(FileExt.lastIndexOf("/") + 1);
            FileName = FileName.substring(0, FileName.length()-extension.length());

            URLConnection conection = url.openConnection();
            conection.connect();
            // getting file length
            int lenghtOfFile = conection.getContentLength();

            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream to write file
            //ToDo: getExternal --> ist das nun intern oder kann das weg?
            String rootPath=Environment.getExternalStorageDirectory()+"/an2ro/podcasticon/";
            File file=new File(rootPath);
            if(!file.exists()){
                file.mkdirs();
            }

            /*bla*/
            int countFileName = 0;

            int fileNumber = 0;

            while(new File(rootPath + fileNumber+"."+extension).exists())
            {
                fileNumber++;
            }
            String FileNameNumber = fileNumber + "."+extension;

            Log.d("blablabla", "FileNameAndExt: " + FileNameNumber);


            OutputStream output = new FileOutputStream(rootPath+FileNameNumber);

            Log.d("blablabla", "FileExt: " + FileExt);

            byte data[] = new byte[1024];

            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;

                // writing data to file
                output.write(data, 0, count);

            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

            returnPath = rootPath+FileNameNumber;
            Log.d("downloadimg", "Pfad: " + returnPath);

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }

        return returnPath;
    }


    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static String getFileName(String _file){
        String completeFileName = "";

        return completeFileName;
    }

    /**
     * After completing background task
     * **/
    @Override
    protected void onPostExecute(String file_url) {


        Log.d("downloadimg", "Bildpfad: " +  returnPath);
    }
}
