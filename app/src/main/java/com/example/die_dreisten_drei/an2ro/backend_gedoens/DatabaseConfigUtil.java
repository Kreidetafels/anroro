package com.example.die_dreisten_drei.an2ro.backend_gedoens;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import static com.j256.ormlite.android.apptools.OrmLiteConfigUtil.writeConfigFile;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

public class DatabaseConfigUtil extends OrmLiteConfigUtil {
    public static void main(String[] args) throws Exception {
        writeConfigFile("ormlite_config.txt");
    }
}
