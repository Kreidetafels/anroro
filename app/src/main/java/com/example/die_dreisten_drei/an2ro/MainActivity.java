/*
@author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

package com.example.die_dreisten_drei.an2ro;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.die_dreisten_drei.an2ro.backend_gedoens.DatabaseHelper;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.EventBus.MessageEvent;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.ItemList;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.Podcast;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.PodcastItem;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.RSSRequest;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.logger.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//ToDo: Option, um Bilder / Streams nur im WLAN runterzuladen
//ToDo: Bilder nachladen, wenn sie gelöscht worden sind / wenn View gewechselt wurde / auf den Homescreen wechselt etc.
//ToDo: http://www.programmierenlernenhq.de/tutorial-android-daten-aktualisieren-mit-swiperefreshlayout/
/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DatabaseHelper databaseHelper = null;
    public static ItemList<Podcast> aboList = null;
    public static MainAdapter aboAdapter;
    public ListView list;
    private Podcast recentlyDeletedPodcast;
    public MainActivity activity = this;

    public static int podcastposition = -1;
    public static int podcastitemposition = -1;

    public final String RSSEND = "RSSend";

    public NavigationView navigationView;
    private DrawerLayout mDrawerLayout;

    public static String searchString = "";

    public FloatingActionButton btnAddPodcast;
    final Context context = this;


  //  private SwipeRefreshLayout mSwipeRefreshLayout;

    //Muss einmal in der App sein, um einen Default-Eventbus zu haben
    EventBus myEventBus = EventBus.getDefault();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = findViewById(R.id.liste);
        list.setDividerHeight(0);
        EventBus.getDefault().register(this); // this == your class instance

//        mSwipeRefreshLayout = findViewById(R.id.container);
//        mSwipeRefreshLayout.setColorScheme(R.color.blue,
//                R.color.green, R.color.orange, R.color.purple);
//        mSwipeRefreshLayout.setOnRefreshListener(this);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.baseline_menu_white_18dp);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        verifyStoragePermissions(this);

        mDrawerLayout = findViewById(R.id.main_drawer_layout);

        btnAddPodcast = findViewById(R.id.btnAddPodcast);




        if (aboList == null) {
            aboList = new ItemList<Podcast>();
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
            final RuntimeExceptionDao<Podcast, Integer> podcastDAO = databaseHelper.getPodcastDao();

            try {
                List<Podcast> templist = podcastDAO.queryForAll();
                aboList.addAll(templist);
            } catch (Exception e) {
                android.util.Log.i(DatabaseHelper.class.getName(), e.getMessage());
            }

        }
        SharedPreferences updateOnStart = getSharedPreferences("prefIDa", Context.MODE_PRIVATE);
        boolean update =updateOnStart.getBoolean("aTrue", false);
        if(update){
            for (Object x : aboList){
                String url=((Podcast)x).getUrl();
                try {
                    new RSSRequest(url, (Podcast)x);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                }
            }
        }

        //            https://fuf.teranas.de/blog/category/podcast/fuf/feed/mp3/
//            http://www.deutschlandfunk.de/podcast-deutschlandfunk-der-tag.3417.de.podcast.xml
        //http://feeds.soundcloud.com/users/soundcloud:users:319180361/sounds.rss
        //http://www.deutschlandfunk.de/podcast-deutschlandfunk-der-tag.3417.de.podcast.xml
        //http://www.sciencemag.org/rss/podcast.xml

        //Log.d("derIntent",getIntent().getStringExtra(Intent.EXTRA_TEXT));
        Intent appintent = getIntent();
        if (appintent.getDataString() != null) {
            String url = appintent.getDataString();
            try {
                new RSSRequest(url);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
        }



        viewAktualisieren();
    }



    @Override
    protected void onResume() {
        super.onResume();
        viewAktualisieren();
    }

    public void viewAktualisieren(){
        aboAdapter = new MainAdapter(this, aboList, getResources());
        list.setAdapter(aboAdapter);

//        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void onItemClick(int mPosition) {
        startActivity(new Intent(this, ChannelActivity.class).putExtra("PODCAST", mPosition));
    }


    //SearchWidget zum Filtern innerhalb der Activity
    //Source: https://developer.android.com/training/appbar/action-views
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        //https://javapapers.com/android/android-searchview-action-bar-tutorial/
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Was passiert bei Textübergabe (nachdem Enter/Suchlupe gedrückt wurde)

                //Query String, der im Moment im SearchView steht
                searchString = (String) searchView.getQuery().toString();
                aboAdapter = new MainAdapter(activity, aboList, getResources());
                list.setAdapter(aboAdapter);
                // Test Ausgabe als Toast
//                Toast.makeText(getApplicationContext(), searchString, Toast.LENGTH_SHORT).show();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // was passiert, wenn der Text geändert wird
                // hier müsste dann die Suchfunktion rein, wenn wir das als "live search" machen wöllten

                if(((String) searchView.getQuery().toString()).isEmpty()) {
                    searchString = "";
                    aboAdapter = new MainAdapter(activity, aboList, getResources());
                    list.setAdapter(aboAdapter);

                }
                return false;
            }
        });


        return true;
    }

    /*
            öffnet ein Dialog, in dem eine RSS Url eingefügt werden kann, um einen Podcast manuell einfügen zu können
            Source: https://www.mkyong.com/android/android-custom-dialog-example/
        */
    public void onClickAddPodcast (View view) {
        final Dialog dialog;
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.add_rss_dialog);

        // set the custom dialog components
        TextView info = (TextView) dialog.findViewById(R.id.info);
        final TextView urlInput = (TextView) dialog.findViewById(R.id.urlInput);
        Button btnCancelDialog = (Button) dialog.findViewById(R.id.btnCancelDialog);
        Button btnAddDialog = (Button) dialog.findViewById(R.id.btnAddDialog);

        // schließen des Dialog Fensters
        btnCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // fügt Podcast anhand des eingegebenen Links hinzu und schließt anschließend das Dialog Fenster
        btnAddDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dialogUrl = "";
                dialogUrl = (String) urlInput.getText().toString();
                android.util.Log.d("rssadd", "Bist im onClick, URL lautet: " + dialogUrl);
                if (dialogUrl.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Ups, nichts eingegeben :(", Toast.LENGTH_SHORT).show();
                    android.util.Log.d("rssadd", "Bist im URL empty URL lautet: " + dialogUrl);

                } else {
                    try {
                        new RSSRequest(dialogUrl);
                        android.util.Log.d("rssadd", "Bist im Try-RSSRequest, URL lautet: " + dialogUrl);
                    }
                    catch (IOException e) {
                        Toast.makeText(getApplicationContext(), "IOException", Toast.LENGTH_SHORT).show();
                        android.util.Log.d("rssadd", "Bist im IOException, URL lautet: " + dialogUrl);

                    } catch (SAXException e) {
                        Toast.makeText(getApplicationContext(), "SAXException", Toast.LENGTH_SHORT).show();
                        android.util.Log.d("rssadd", "Bist im SAXException, URL lautet: " + dialogUrl);

                    }
                    dialog.dismiss();
                    viewAktualisieren();

                }
            }
        });

        dialog.show();
    }

    //NavigationDrawer Gedöns
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Menu menuNav = navigationView.getMenu();
        MenuItem nav_podcasts = menuNav.findItem(R.id.nav_podcasts);
        nav_podcasts.setEnabled(false);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_podcasts) {

        } else if (id == R.id.nav_player) {
            //todo hinbekommen, dass man den Player wieder aufrufen kann (geht momentan nicht, weil er die Position des Items zum Aufruf der PlayerActivity benötigt)
            Intent toPlayer = new Intent(this, PlayerActivity.class);
            /*if(PlayerActivity.isPlaying) {
                startActivity(toPlayer);
            } else {
                Toast.makeText(this, "Zur Zeit wird kein podcast abgespielt", Toast.LENGTH_SHORT).show();
            }*/
        } else if (id == R.id.nav_offlinePodcasts) {
            Intent toDownloadedfiles = new Intent(this, Downloadedfiles.class);
            startActivity(toDownloadedfiles);
        } else if (id == R.id.nav_settings) {
            Intent toSettings = new Intent(this, SettingsActivity.class);
            startActivity(toSettings);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void btnDelete(int _position) {

        recentlyDeletedPodcast = aboList.get(_position);
        File halfDeathImage = new File(recentlyDeletedPodcast.getImage());
        if(halfDeathImage.exists()){
            halfDeathImage.delete();
        }
        for (PodcastItem x: recentlyDeletedPodcast.getStreams()){

            if(x.getOld_url()!=null){
                File forDeath = new File(x.getPath());
                if(forDeath.exists()){
                    forDeath.delete();
                }
            }
        }
        if (databaseHelper == null)
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        final RuntimeExceptionDao<Podcast, Integer> podcastDAO = databaseHelper.getPodcastDao();
        podcastDAO.delete(recentlyDeletedPodcast);
        aboList.remove(_position);
// das oder das danach?        aboAdapter = new MainAdapter(activity, aboList, getResources());
        aboAdapter = new MainAdapter(this, aboList, getResources());
        list.setAdapter(aboAdapter);
        Toast.makeText(this, recentlyDeletedPodcast.getTitle()+ " wurde entfernt.", Toast.LENGTH_LONG).show();
    }

    public void btnUpdate(int _position){

        String url = aboList.get(_position).getUrl();

        try {
            new RSSRequest(url, aboList.get(_position));
            Toast.makeText(this, aboList.get(_position).getTitle()+ " wurde aktualisiert.", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
            //Android-App einmal restarten, nachdem nach den Permissions gefragt wurde.
            //Ist ein nerviger Bug, der kommt, wenn man eine App direkt aus der IDE startet.
            //Koennt evtl. auch wieder raus, nun ja. Daumen druecken, ansonsten wissen Sie ja nun, dass das eine Fehlerquelle ist, die sich schnell beheben ließe. :)
           // android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    @Subscribe
    public void onEvent(MessageEvent event) {
        switch (event.getMessage().toString()) {
            case RSSEND:
                viewAktualisieren();
//                Toast.makeText(activity, "lecko", Toast.LENGTH_SHORT).show();
        }
    }



}
