package com.example.die_dreisten_drei.an2ro.backend_gedoens;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.example.die_dreisten_drei.an2ro.MainActivity;
import com.example.die_dreisten_drei.an2ro.R;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.EventBus.MessageEvent;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import org.greenrobot.eventbus.EventBus;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

public class RSSContentHandler implements ContentHandler {

    private Podcast tempcast = new Podcast();
    private StringBuilder currentValue = new StringBuilder();
    private boolean inItem = false;
    private PodcastItem podcastItem;


    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {

        if (localName.equals("item")) {
            inItem = true;
            podcastItem = new PodcastItem();

        }


        if (localName.contains("image") && !inItem) {
            if (atts.getLength() != 0) {
                String _localNameUrl = atts.getValue("href");
                // new DownloadImageTask().execute(_localNameUrl);

                DownloadImageTask dit = new DownloadImageTask();
                dit.execute(_localNameUrl);
                String bla = null;
                while (bla == null || bla.isEmpty())
                    bla = dit.returnPath;
                //dit.doInBackground(atts.getValue("href"));
                //Downloade hier nun das JPG.
                //Wenn Bild nicht vorhanden, lade es runter, ansonsten lade es aus dem gespeicherten Ordner
                //z.B. an2ro/podcast/image
                //Pfad zum Bild setzen und gleichzeitig DL starten
                String rootPath = Environment.getExternalStorageDirectory() + "/an2ro/podcasticon/";
                Log.d("downloadimg", "RSSContentHandler: returnpath: " + bla);
                tempcast.setImage(bla);
            }
        }

        if (localName.equals("enclosure")) {
            podcastItem.setPath(atts.getValue("url"));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        String tempString = currentValue.toString().trim();
        if (tempString.contains("<") && tempString.contains(">")) {
            tempString = tempString.replaceAll("<br/>", "\n\n").replaceAll("\\<.*?\\>", "");
        }

        currentValue.setLength(0);
        if (localName.equals("an2ropodcasturl")) {
            tempcast.setUrl(tempString);
        }

        if (localName.equals("title")) {
            if (!inItem) {
                tempcast.setTitle(tempString);
            } else {
                podcastItem.setTitle(tempString);
            }
        }

        if (localName.contains("author")) {
            if (!inItem) {
                tempcast.setAuthor(tempString);
            } else {
                podcastItem.setAuthor(tempString);
            }
        }

        if (localName.contains("description")) {
            if (!inItem) {
                tempcast.setDesc(tempString);
            } else {
                podcastItem.setDescription(tempString);
            }
        }

        if (localName.contains("duration")) {
            podcastItem.setDuration(tempString);
        }

        if (localName.equals("item")) {
            inItem = false;
            podcastItem.setPodcast_image(tempcast.getImage());
            tempcast.getStreams().add(podcastItem);
        }

        if(localName.equals("lastBuildDate")){
            tempcast.setDate(tempString);
        }
    }

    @Override
    public void characters(char[] chars, int start, int length) throws SAXException {
        currentValue.append(chars, start, length);
    }

    @Override
    public void startDocument() throws SAXException {

    }

    @Override
    public void endDocument() throws SAXException {
        MainActivity.aboList.add(tempcast);
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(new MainActivity(), DatabaseHelper.class);
        final RuntimeExceptionDao<Podcast, Integer> podcastDAO = databaseHelper.getPodcastDao();
        podcastDAO.create(tempcast);


        EventBus.getDefault().post(new MessageEvent("RSSend"));

    }

    @Override
    public void startPrefixMapping(String s, String s1) throws SAXException {
    }

    @Override
    public void endPrefixMapping(String s) throws SAXException {
    }

    @Override
    public void ignorableWhitespace(char[] chars, int i, int i1) throws SAXException {
    }

    @Override
    public void processingInstruction(String s, String s1) throws SAXException {
    }

    @Override
    public void skippedEntity(String s) throws SAXException {
    }

    @Override
    public void setDocumentLocator(Locator locator) {
    }

}
