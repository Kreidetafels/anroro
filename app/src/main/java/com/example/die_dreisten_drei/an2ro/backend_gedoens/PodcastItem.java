package com.example.die_dreisten_drei.an2ro.backend_gedoens;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

import static com.j256.ormlite.field.DataType.SERIALIZABLE;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

@DatabaseTable(tableName = "PodcastItems")
public class PodcastItem implements Serializable {
    @DatabaseField(generatedId = true)
    int id;
    @DatabaseField
    private String podcast_image;
    @DatabaseField(index = true)
    private String title;
    @DatabaseField
    private String path;
    @DatabaseField
    private String old_url;
    @DatabaseField
    private String author;
    @DatabaseField
    private String description;
    @DatabaseField
    private String duration;
    @DatabaseField
    private long lastPosition=0;

    public PodcastItem(String _title, String _path, String _author, String _description){
        this.setTitle(_title);
        this.setPath(_path);
        this.setAuthor(_author);
        this.setDescription(_description);
    }

    public PodcastItem(){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getOld_url() {
        return old_url;
    }

    public void setOld_url(String old_url) {
        this.old_url = old_url;
    }

    public String getPodcast_image() {
        return podcast_image;
    }

    public void setPodcast_image(String podcast_image) {
        this.podcast_image = podcast_image;
    }

    public long getLastPosition() {
        return lastPosition;
    }

    public void setLastPosition(long lastPosition) {
        this.lastPosition = lastPosition;
    }

    public String toString(){
        return this.getTitle();
    }
}
