package com.example.die_dreisten_drei.an2ro.backend_gedoens.EventBus;

/* Created by Robert Neumann on 07.06.2018.
 * Matrikel-Nr.: 15589
 * ToDo:
 */
public class MessageEvent {
    public final String message;


    public MessageEvent(String _message){
        this.message = _message;
    }


    public String getMessage(){
        return message;
    }

}
