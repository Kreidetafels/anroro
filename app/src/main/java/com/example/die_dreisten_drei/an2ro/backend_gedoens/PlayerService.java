package com.example.die_dreisten_drei.an2ro.backend_gedoens;

import android.app.DownloadManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.example.die_dreisten_drei.an2ro.MainActivity;
import com.example.die_dreisten_drei.an2ro.PlayerActivity;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.EventBus.MessageEvent;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.EventBus.SeekbarEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */
/*
 * ToDo: https://github.com/IrosTheBeggar/mstream-android-app/blob/master/app/src/main/java/io/mstream/mstream/PlayerControlsFragment.java
 * ToDo: Am Handy klappt es noch nicht so recht. Vlt. Buffer einbauen?
 * ToDo: long nutzen, um die Abspielpositon zu speichern
 * ToDo: onFinish*/
public class PlayerService extends Service implements AudioManager.OnAudioFocusChangeListener{
    private Handler handler = new Handler();
    private int podcast_listPosition;
    private int podcast_itemListPosition;
    private PodcastItem podcastItem;
    int LenTimeInMs = 0;
    int PosTimeInMS = 0;
    //MediaPlayer init
    private MediaPlayer mediaPlayer;


    private String url;
    String playURL = "";
    boolean end = false;
    AudioManager audioManager;
    public final static String MY_ACTION = "MY_ACTION";

    //EventBus SwitchCases
    public final String EB_STARTPODCAST = "START_PODCAST";
    public final String EB_PAUSEPODCAST = "PAUSE_PODCAST";
    public final String EB_SEEKFORWARD = "SEEK_FORWARD";
    public final String EB_SEEKBACK = "SEEK_BACK";
    public final String EB_DOWNLOADPODCAST = "DOWNLOAD_PODCAST";
    public final String TAG = "playerservice";

    boolean playerInit = false;

    final static String INVALID_URL = "INVALID_URL";
    private LocalBroadcastManager mBroadcastManger;

    final static String ServLogTag = "ServiceLogTag";
    Context context; // before onCreate in MainActivity

    //Leerer Konstruktor
    public PlayerService() {
    }

    public int getLength(){
        if(mediaPlayer!=null){
        LenTimeInMs = mediaPlayer.getDuration();}
        return LenTimeInMs;
    }


    public int getCurrentPosition(){
        if(mediaPlayer!=null){
            PosTimeInMS = mediaPlayer.getCurrentPosition();}
        return PosTimeInMS;
    }
    public void onDestroy() {
        super.onDestroy();


        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
        playerInit = false;

        stopService(new Intent(getApplicationContext(), PlayerService.class));
        EventBus.getDefault().unregister(this);
    }


    //Event muss subscribt werden; im event ist momentan nur ein String für die Switch-Cases
    @Subscribe
    public void onEvent(MessageEvent event) {
        switch (event.getMessage().toString()) {
            case EB_STARTPODCAST:
                Log.e("onEvent", "bist im EB_START, Player wurde initialisiert");
                if(playerInit) {
                playMedia();
                mediaPlayer.start();
                break;

                }else if(playerInit) {
                    Log.e("onEvent", "bist im EB_START, Player wurde noch nicht initialisiert");
                    initMediaPlayer();
                    break;
                }

            case EB_PAUSEPODCAST:
                Log.e("bla", "bist im EB_PAUSE");
                mediaPlayer.pause();

                break;


            case EB_SEEKBACK:
                if (mediaPlayer.getCurrentPosition() > 5000){
                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - 10000);}
                else{
                    mediaPlayer.seekTo(0);}
                break;

            case EB_SEEKFORWARD:
                if (mediaPlayer.getDuration() - mediaPlayer.getCurrentPosition() > 5000)
                    mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + 5000);

                break;


            case EB_DOWNLOADPODCAST:
                Log.e("bla", "bist im EB_DLPODCAST");

                DownloadPodcastTask dlpodcast = new DownloadPodcastTask(getApplicationContext());
            dlpodcast.execute(playURL, Integer.toString(podcast_listPosition), Integer.toString(podcast_itemListPosition));

             //   downloadFile(playURL);
                Log.d(TAG, "onEvent: " + playURL);
                break;

        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        podcast_listPosition= intent.getIntExtra("PODCAST_LISTPOSITION",0);
        podcast_itemListPosition= intent.getIntExtra("PODCAST_ITEMLISTPOSITION",0);


        podcastItem = MainActivity.aboList.get(podcast_listPosition).getStreams().get(podcast_itemListPosition);

        if(podcastItem.getLastPosition()!=0) {
            seekTo((int) podcastItem.getLastPosition());
        }
        Log.d(TAG, "onStartCommand: getLastPosi" + podcastItem.getLastPosition() );

        //       playURL = podcastItem.getPodcast_url();
        Log.d(TAG, "onStartCommand: " + podcastItem);
       playURL = intent.getStringExtra("INTENT_URL").toString();
        initMediaPlayer();

    //   return super.onStartCommand(intent, flags, startId);
        /*ansonsten Absturz, wenn App mit Task-Manager gekillt wird, da Android versucht
        * den Service neu zu starten und einen leeren Intent mitgibt
        * Alternativ: START_NOT_STICKY, müssen wir nochmal schauen*/

        return START_REDELIVER_INTENT;
    }

    //Platzhalter für später, falls man noch Infos zur PlayerAct. schicken will
    private void sendMessage(int _progressNumber) {
        Intent intent = new Intent(MY_ACTION);
        intent.putExtra(MY_ACTION, String.valueOf(_progressNumber));
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }



    @Override
    public void onCreate() {
        super.onCreate();

        mBroadcastManger = LocalBroadcastManager.getInstance(this);
        EventBus.getDefault().register(this); // this == your class instance
    }


    //Objekt erstellen, um den Client an den Service zu binden
    private final IBinder iBinderTutorial = new MyLocaleBinder();


    public void initMediaPlayer() {

        if(playerInit && mediaPlayer != null)
            mediaPlayer.stop();

        mediaPlayer = new MediaPlayer();
        playerInit = true;
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            mediaPlayer.setDataSource(playURL);

        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (SecurityException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            mediaPlayer.prepare();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        }

        //Podcast-Laenge bekommen
        getLength();

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.seekTo(0);
                mediaPlayer.stop();
            }

        });

    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onMessageEvent(SeekbarEvent.SeekTo event) {
        seekTo(event.position);
        Log.d("seekbarevent", "seekbarevent: " + event.position);
    }


    public void seekTo(int duration) {
        mediaPlayer.seekTo(duration);
    }




    public void playMedia() {


        if(!mediaPlayer.isPlaying()) {


            new Thread(new Runnable() {
                @Override
                public void run() {
                    //Can't create handler inside thread that has not called Looper.prepare()
                    Looper.prepare();
                    try {
                        mediaPlayer.start();
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        playerInit = true;
                    } catch (Exception e) {
                        Log.e("playerservice", "Bitter: " + e.getMessage());
                    }
                }
            }).start();
        }
    }


    //Wird zu start aufgerufen, was soll er machen, wenn er bindet? --> return iBinderTutorial, was nur eine Instan von MyLocaleBinder ist
    //Android is stupid
    @Override
    public IBinder onBind(Intent intent) {
        return iBinderTutorial;
    }


    //1. Wenn man Client-Service binden möchte --> Objekt erstellen durch Binder erweitert -> nun kann er 2 Dinge zusammenbinden
    public class MyLocaleBinder extends Binder {
        //2. LocalBinder returnt nur Referenz zur Mutterklasse (MyService), damit wir die Methoden in MyService nutzen können
        public PlayerService getService() {
            return PlayerService.this;
        }
    }

  // ToDo: Überarbeiten --> Funktion, um andere AudioEvents zu handhaben (bspw. während Podcasts wird ein anderer Sound gespielt)
    /* *  ------------------------------------------------
     *  -            Audio Manager Methods             -
     *  ------------------------------------------------
     */
    // Method to release all resources taken by the mp object*/
    public void releaseMediaPlayer() {
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
    }


    // https://stackoverflow.com/questions/37499177/using-mediaplayer-in-a-service-class
    public void onAudioFocusChange(int focusChange) {
        // Do something based on focus change...
        switch (focusChange) {

            // Case if we have gained the focus to play!
            case AudioManager.AUDIOFOCUS_GAIN:
                if (this.mediaPlayer == null)
                    initMediaPlayer(); // If we had to release the mp by any reason, we re-initialize it!
                else if (!this.mediaPlayer.isPlaying()) {
                    this.mediaPlayer.setLooping(true);  // In theory, it should play looping through infinitely
                    this.mediaPlayer.start(); // If it's not currently playing, starts!
                }
                this.mediaPlayer.setVolume(1.0f, 1.0f);  // We set the volume!
                break;

            // Case if we have completely (or almost) lost the focus
            case AudioManager.AUDIOFOCUS_LOSS:
                if (this.mediaPlayer.isPlaying())
                    this.mediaPlayer.stop(); // If it's currently playing, we have to stop it first
                end = true;
                releaseMediaPlayer();  // We release the resources
                break;

            // Case if we have lost the focus for a short period of time
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                if (this.mediaPlayer.isPlaying())
                    this.mediaPlayer.pause(); // If it's currently playing, we just pause it!
                break;

            // Case if we have lost the focus but for a tiny piece of time, so we're allowed to continue performing the music but in a quiet state!
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                if (this.mediaPlayer.isPlaying())
                    this.mediaPlayer.setVolume(0.3f, 0.3f); // If it's currently playing, we just set the volume to a quiet level!
                break;
        }
    }

}
