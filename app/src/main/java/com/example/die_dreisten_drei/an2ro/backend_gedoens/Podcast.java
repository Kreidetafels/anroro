package com.example.die_dreisten_drei.an2ro.backend_gedoens;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static com.j256.ormlite.field.DataType.SERIALIZABLE;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

@DatabaseTable(tableName = "Podcasts")
public class Podcast implements Serializable {
    @DatabaseField(generatedId = true)
    int id;
    @DatabaseField(index = true)
    private String title;
    @DatabaseField
    private String guid;
    @DatabaseField
    private String date;
    @DatabaseField
    private String desc;
    @DatabaseField
    private String image;
    @DatabaseField
    private String url;
    @DatabaseField(dataType = SERIALIZABLE)
    public ArrayList<PodcastItem> streams = new ArrayList<PodcastItem>();
    @DatabaseField
    private String author;
    @DatabaseField
    private boolean hasDownloads;



    public Podcast(String _title, String _desc, String _image, String _author) {
        if (_title != null && !_title.isEmpty()) {
            setTitle(_title);
            setDesc(_desc);
            setImage(_image);
            setAuthor(_author);
        }
    }

    public Podcast() {}

    public ArrayList<PodcastItem> getStreams() {
        return streams;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        if (desc == null || desc.isEmpty())
            desc = new String("keine Beschreibung verfügbar");
        this.desc = desc;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        if (author == null || author.isEmpty())
            author = new String("Unbekannter Author");
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean hasDownloads() {
        return hasDownloads;
    }

    public void setHasDownloads(boolean hasDownloads) {
        this.hasDownloads = hasDownloads;
    }

    public void addItem(PodcastItem _item) {
        this.streams.add(_item);
    }


    public String toString() {
        return this.getTitle();
    }
}
