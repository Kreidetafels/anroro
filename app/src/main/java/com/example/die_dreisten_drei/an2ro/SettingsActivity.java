package com.example.die_dreisten_drei.an2ro;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.die_dreisten_drei.an2ro.backend_gedoens.EventBus.MessageEvent;

import org.greenrobot.eventbus.EventBus;
/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */
public class SettingsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public NavigationView navigationView;
    private DrawerLayout mDrawerLayout;

    final Context context = this;

    Switch notificationsSwitch;
    static boolean notificationStatus;
    Switch aktualisierenSwitch;

    SharedPreferences notificationPrefs;
    SharedPreferences aktualisierenPrefs;
    SharedPreferences sleeptimePrefs;

    TextView sleepTimeInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.baseline_menu_white_18dp);

        navigationView = (NavigationView) findViewById(R.id.nav_view4);
        navigationView.setNavigationItemSelectedListener(this);

        mDrawerLayout = findViewById(R.id.settings_drawer_layout);

        notificationsSwitch = findViewById(R.id.notificationsSwitch);
        aktualisierenSwitch = findViewById(R.id.aktualisierenSwitch);

        sleepTimeInput = findViewById(R.id.sleeptimeInput);

        // https://appsandbiscuits.com/saving-data-with-sharedpreferences-android-9-9fecae19896a
        notificationPrefs = getSharedPreferences("prefIDn", Context.MODE_PRIVATE);
        aktualisierenPrefs = getSharedPreferences("prefIDa", Context.MODE_PRIVATE);
        sleeptimePrefs = getSharedPreferences("prefIDs", Context.MODE_PRIVATE);

        sleepTimeInput.setText(sleeptimePrefs.getString("sleepy", "5"));

        atNotificationsSwitch();
        atAktualisierenSwitch();

    }

    //NavigationDrawer Gedöns
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Menu menuNav = navigationView.getMenu();
        MenuItem nav_settings = menuNav.findItem(R.id.nav_settings);
        nav_settings.setEnabled(false);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_podcasts) {
            Intent podcastIntent = new Intent(this, MainActivity.class);
            startActivity(podcastIntent);
        } else if (id == R.id.nav_player) {

        } else if (id == R.id.nav_offlinePodcasts) {
            Intent toDownloadedfiles = new Intent(this, Downloadedfiles.class);
            startActivity(toDownloadedfiles);
        } else if (id == R.id.nav_settings) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.settings_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*
        händeln des Notification Switches
     */
    public void atNotificationsSwitch() {
        notificationsSwitch.setChecked(notificationPrefs.getBoolean("nTrue", true));

        notificationsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences.Editor editor = notificationPrefs.edit();
                    editor.putBoolean("nTrue", true);
                    editor.apply();

                    notificationStatus = true;

                    Toast.makeText(getApplicationContext(), "Notification Player ist aktiviert", Toast.LENGTH_SHORT).show();

                } else {
                    SharedPreferences.Editor editor = notificationPrefs.edit();
                    editor.putBoolean("nTrue", false);
                    editor.apply();

                    notificationStatus = false;

                    Toast.makeText(getApplicationContext(), "Notification Player wurde deaktiviert", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    //todo Aktualisierungsfunktion einbauen
    public void atAktualisierenSwitch() {
        aktualisierenSwitch.setChecked(aktualisierenPrefs.getBoolean("aTrue", false));

        aktualisierenSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences.Editor editor = aktualisierenPrefs.edit();
                    editor.putBoolean("aTrue", true);
                    editor.apply();

                    Toast.makeText(getApplicationContext(), "Podcasts werden bei Appstart aktualisiert", Toast.LENGTH_SHORT).show();

                } else {
                    SharedPreferences.Editor editor = aktualisierenPrefs.edit();
                    editor.putBoolean("aTrue", false);
                    editor.apply();

                    Toast.makeText(getApplicationContext(), "Podcasts müssen manuell aktualisiert werden", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    /*
        erstellt bei Klick auf Layoutelement ein Dialog mit Infotext
     */
    public void onClickInfodialog(View v) {
        final Dialog dialog;
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.settings_info_dialog);

        // set the custom dialog components
        TextView infotext = (TextView) dialog.findViewById(R.id.infotext);
        infotext.setText("Das Laden der Poscasts beim Start der App kann zu hohen Ladezeiten führen. Alternativ können Podcasts auch manuell nachgeladen werden, um Ressourcen zu sparen.");

        dialog.show();
    }

    public void onClickSetTimer(View v) {
        String sleepTime = (String) sleepTimeInput.getText().toString();



        if (!sleepTime.isEmpty()) {
            Toast.makeText(this, "Der Player wird in " + sleepTime + " min pausiert", Toast.LENGTH_LONG).show();

            gehSchlafen((Integer.parseInt(sleepTime))*1000 * 60);
        } else {
            Toast.makeText(this, "Bitte geben Sie einen Wert in Minuten ein", Toast.LENGTH_SHORT).show();
        }

        SharedPreferences.Editor editor = sleeptimePrefs.edit();
        editor.putString("sleepy", sleepTime);
        editor.apply();
    }

    public void gehSchlafen (int _i) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new MessageEvent(getString(R.string.eb_pausepodcast)));
            }
        }, _i);
    }
}

