package com.example.die_dreisten_drei.an2ro;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.die_dreisten_drei.an2ro.backend_gedoens.DatabaseHelper;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.Podcast;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.PodcastItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainAdapter extends BaseAdapter implements View.OnClickListener, View.OnLongClickListener {

    private Activity activity;
    private ArrayList data;
    private static LayoutInflater inflater = null;
    private Resources res;
    private Podcast tempCast = null;
    private int i = 0;

    public MainAdapter(Activity _activity, ArrayList<Podcast> _arraylist, Resources _resLocal) {
        this.activity = _activity;
        data = new ArrayList();
        for (Podcast x : _arraylist)
            if (x.getTitle().toLowerCase().contains(MainActivity.searchString.toLowerCase()))
                data.add(x);
        res = _resLocal;
        inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder {


        ImageView imgMainIcon;
        TextView tfMainTitle;
        ImageView imgMainSubDivider;
        TextView tfMainDescription;
        ImageView imgMainDivider;

    }


    private Context context;

    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.mainlistlayout, null);
            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.imgMainIcon = vi.findViewById(R.id.imgMainIcon);
            holder.tfMainTitle = vi.findViewById(R.id.tfMainTitle);
            holder.imgMainSubDivider = vi.findViewById(R.id.imgMainSubDivider);
            holder.tfMainDescription = vi.findViewById(R.id.tfMainDescription);
            holder.imgMainDivider = vi.findViewById(R.id.imgMainDivider);

            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (data.size() <= 0) {
            holder.tfMainTitle.setText("No Data");

        } else {
            /***** Get each Model object from Arraylist ********/
            tempCast = null;
            tempCast = (Podcast) data.get(position);

            /************  Set Model values in Holder elements ***********/

            Picasso.get()
                    .load("file://"+tempCast.getImage())
                    .placeholder(R.drawable.placeholder_image)
                    .fit()
                    .into(holder.imgMainIcon);

            holder.tfMainTitle.setText(tempCast.getTitle());
            holder.imgMainSubDivider.setBackgroundColor(vi.getResources().getColor(R.color.an2roLight));
            holder.tfMainDescription.setText(tempCast.getDesc());
            holder.imgMainDivider.setBackgroundColor(vi.getResources().getColor(R.color.an2roLight));


//            holder.image.setImageResource(
//                    res.getIdentifier(
//                            "com.androidexample.customlistview:drawable/"+tempCast.getImage()
//                            ,null,null));

            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new OnItemClickListener(position));
            vi.setOnLongClickListener(new OnItemLongClickListener(position));
        }
        return vi;
    }



    @Override
    public void onClick(View v) {
        Log.v("CustomAdapter", "=====Row button clicked=====");
    }

    @Override
    public boolean onLongClick(View v) {
        return true;
    }

    private class OnItemLongClickListener implements View.OnLongClickListener {
        private int pos;

        OnItemLongClickListener(int _position) {
            pos = _position;
        }

        @Override
        public boolean onLongClick(View v) {
            PopupMenu popupMenu = new PopupMenu((MainActivity) activity, v);
            MenuInflater inflater = popupMenu.getMenuInflater();
            inflater.inflate(R.menu.menu_longclick_main, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_update:
                            ((MainActivity) activity).btnUpdate(pos);
                            return true;
                        case R.id.menu_delete:
                            ((MainActivity) activity).btnDelete(pos);
                            return true;
                        default:
                            return false;
                    }
                }
            });
            popupMenu.show();


            Log.i(DatabaseHelper.class.getName(), "longClick");
            return true;
        }
    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener implements View.OnClickListener {
        private int pos;

        OnItemClickListener(int _position) {
            if (MainActivity.searchString.isEmpty()) {
                pos = _position;
            } else {
                for (int i = 0; i < MainActivity.aboList.size(); i++) {
                    if(MainActivity.aboList.get(i).getUrl().equals(((Podcast)data.get(_position)).getUrl())){
                        pos=i;
                        break;
                    }
                }
            }

        }

        @Override
        public void onClick(View _arg0) {


            MainActivity sct = (MainActivity) activity;

            /****  Call  onItemClick Method inside CustomListViewAndroidExample Class ( See Below )****/


            sct.onItemClick(pos);
        }
    }
}
