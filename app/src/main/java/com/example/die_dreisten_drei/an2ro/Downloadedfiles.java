package com.example.die_dreisten_drei.an2ro;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */
public class Downloadedfiles extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DownloadedfilesAdapter pcItems;

    public NavigationView navigationView;
    private DrawerLayout mDrawerLayout;
    Downloadedfiles activity = this;
    private ListView list;

    public static String searchString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloadedfiles);
        list = findViewById(R.id.list_dl);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.baseline_menu_white_18dp);

        navigationView = (NavigationView) findViewById(R.id.nav_view4);
        navigationView.setNavigationItemSelectedListener(this);

        mDrawerLayout = findViewById(R.id.downloadedfiles_drawer_layout);
        pcItems = new DownloadedfilesAdapter(this, MainActivity.aboList, getResources());
        list.setAdapter(pcItems);
    }

    public void refreshView(){
        pcItems = new DownloadedfilesAdapter(this, MainActivity.aboList, getResources());
        list.setAdapter(pcItems);
    }

    public void playItem(int podcastPosition ,int itemPosition){
        startActivity(new Intent(this, PlayerActivity.class).putExtra("PODCAST", podcastPosition).putExtra("ITEMPOSITION", itemPosition));
    }

    //SearchWidget zum Filtern innerhalb der Activity
    //Source: https://developer.android.com/training/appbar/action-views
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        //https://javapapers.com/android/android-searchview-action-bar-tutorial/
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Was passiert bei Textübergabe (nachdem Enter/Suchlupe gedrückt wurde)

                //Query String, der im Moment im SearchView steht
                searchString = (String) searchView.getQuery().toString();
                pcItems = new DownloadedfilesAdapter(activity, MainActivity.aboList, getResources());
                list.setAdapter(pcItems);
//                aboAdapter = new MainAdapter(activity, aboList, getResources());
//                list.setAdapter(aboAdapter);
                // Test Ausgabe als Toast
//                Toast.makeText(getApplicationContext(), searchString, Toast.LENGTH_SHORT).show();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // was passiert, wenn der Text geändert wird
                // hier müsste dann die Suchfunktion rein, wenn wir das als "live search" machen wöllten

                if(((String) searchView.getQuery().toString()).isEmpty()) {
                    searchString = "";
                    pcItems = new DownloadedfilesAdapter(activity, MainActivity.aboList, getResources());
                    list.setAdapter(pcItems);
//                    aboAdapter = new MainAdapter(activity, aboList, getResources());
//                    list.setAdapter(aboAdapter);

                }
                return false;
            }
        });


        return true;
    }

    //NavigationDrawer Gedöns
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Menu menuNav = navigationView.getMenu();
        MenuItem nav_offlinePodcasts = menuNav.findItem(R.id.nav_offlinePodcasts);
        nav_offlinePodcasts.setEnabled(false);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_podcasts) {
            Intent podcastIntent = new Intent(this, MainActivity.class);
            startActivity(podcastIntent);
        } else if (id == R.id.nav_player) {

        } else if (id == R.id.nav_offlinePodcasts) {

        } else if (id == R.id.nav_settings) {
            Intent toSettings = new Intent(this, SettingsActivity.class);
            startActivity(toSettings);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.downloadedfiles_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
