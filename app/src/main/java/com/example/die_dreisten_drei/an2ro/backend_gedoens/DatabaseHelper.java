package com.example.die_dreisten_drei.an2ro.backend_gedoens;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe und Stackoverflow
 */

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.die_dreisten_drei.an2ro.MainActivity;
import com.example.die_dreisten_drei.an2ro.R;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    //ToDo: test, um nur eine Instanz aufzurufen
    // http://touchlabblog.tumblr.com/post/24474750219/single-sqlite-connection

    // name of the database file for your application -- change to something appropriate for your app
    private static final String DATABASE_NAME = "anroro.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    // the DAO object we use to access the Podcast table
    private Dao<Podcast, Integer> simpleDao = null;
    private RuntimeExceptionDao<Podcast, Integer> simpleRuntimeDao = null;
    private Dao<Podcast, Integer> aNull;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Podcast.class);
            TableUtils.createTable(connectionSource, PodcastItem.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }


    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Podcast.class, true);
            TableUtils.dropTable(connectionSource, PodcastItem.class, true);

            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the Database Access Object (DAO) for our Podcast class. It will create it or just give the cached
     * value.
     */
    public Dao<Podcast, Integer> getDao() throws SQLException {
        if (simpleDao == null) {
            simpleDao = getDao(Podcast.class);
        }
        return simpleDao;
    }


    public RuntimeExceptionDao<Podcast, Integer> getPodcastDao() {

        return getRuntimeExceptionDao(Podcast.class);
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
//
//                // here we try inserting data in the on-create as a test
//        RuntimeExceptionDao<Podcast, Integer> dao = getPodcastDao();
//        // create some entries in the onCreate
//        for (int i =0; i<MainActivity.aboList.size(); i++){
//            dao.create(MainActivity.aboList.get(i));
//        }
//
//
//
//        Log.i(DatabaseHelper.class.getName(), "created new entries in onCreate: ");

        super.close();
//        simpleDao = null;
//        simpleRuntimeDao = null;
    }
}