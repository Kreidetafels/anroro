#
# generated on 2018/06/25 05:36:40
#
# --table-start--
dataClass=com.example.die_dreisten_drei.an2ro.backend_gedoens.Podcast
tableName=Podcasts
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --field-start--
fieldName=title
indexName=Podcasts_title_idx
# --field-end--
# --field-start--
fieldName=guid
# --field-end--
# --field-start--
fieldName=date
# --field-end--
# --field-start--
fieldName=desc
# --field-end--
# --field-start--
fieldName=image
# --field-end--
# --field-start--
fieldName=url
# --field-end--
# --field-start--
fieldName=streams
dataPersister=SERIALIZABLE
# --field-end--
# --field-start--
fieldName=author
# --field-end--
# --field-start--
fieldName=hasDownloads
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=com.example.die_dreisten_drei.an2ro.backend_gedoens.PodcastItem
tableName=PodcastItems
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --field-start--
fieldName=podcast_image
# --field-end--
# --field-start--
fieldName=title
indexName=PodcastItems_title_idx
# --field-end--
# --field-start--
fieldName=path
# --field-end--
# --field-start--
fieldName=old_url
# --field-end--
# --field-start--
fieldName=author
# --field-end--
# --field-start--
fieldName=description
# --field-end--
# --field-start--
fieldName=duration
# --field-end--
# --field-start--
fieldName=lastPosition
# --field-end--
# --table-fields-end--
# --table-end--
#################################
